package com.example.ppmazer.Controller;

import com.example.ppmazer.Model.Book;
import com.example.ppmazer.Service.BookService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class BookController {
    private BookService bookservice;
    @Autowired
    public BookController(BookService bookservice) {
        this.bookservice = bookservice;
    }
    @GetMapping(value = {"/index","/","/home"})
    public String index(Model model){
        List<Book> disBook=this.bookservice.getBooks();
        model.addAttribute("books",disBook);
        return "index";
    }
    @GetMapping("/newBook")
    public String addNewPage(Model model){
        model.addAttribute("book",new Book());
        return "addnewbook";
    }
    @PostMapping("/addnew/summit")
    public String addNewBook(@ModelAttribute Book book){
        this.bookservice.addBook(book);
        return "redirect:/home";
    }
    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") int id,Model model){
        Book bookResult=this.bookservice.searchBook(id);
        System.out.println(id);
        System.out.println(bookResult);
        model.addAttribute("viewbook",bookResult);
        return "viewbook";
    }
    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") int id,Model model){
        Book searchbook=this.bookservice.searchBook(id);
        model.addAttribute("result",searchbook);
        return "updateBook";
    }
    @PostMapping("/update/submit")
    public String update(@Valid Book book){
        this.bookservice.updateBook(book);
        return "redirect:/home";
    }
    @GetMapping("/remove/{id}")
    public String remove(@PathVariable("id") int id){
        this.bookservice.deleteBook(id);
        return "redirect:/index";
    }
    @GetMapping("/count")
    @ResponseBody
    public Map<String, Object> count(){
        Map<String,Object> countlist=new HashMap<>();
        countlist.put("Record_Set",this.bookservice.count());
        countlist.put("Status", true);
        return countlist;
    }
}
