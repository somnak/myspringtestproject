package com.example.ppmazer.Repository;


import com.example.ppmazer.Model.Book;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;
import sun.swing.BakedArrayList;

import java.util.*;

@Repository
public class BookRepository {
    Faker faker=new Faker();
    List<Book> books=new ArrayList<>();
    int bookCurrentId=1;
    {
        for(int i=0;i<10;i++){
            Book book=new Book();
            book.setId(bookCurrentId++);
            book.setTitle(faker.book().title());
            book.setAuthor(faker.book().author());
            book.setPublisher(faker.book().publisher());
            books.add(book);
        }
    }
    public List<Book> getAllBooks(){
        return books;
    }
    public Book searchBook(int id){
        for(int i=0;i<books.size();i++){
            if(books.get(i).getId()==id){
                return books.get(i);
            }
        }
        return null;
    }
    public boolean addBook(Book book){
        return books.add(book);
    }
    public boolean removeBook(int id){
        for(int i=0; i <books.size();i++){
            if(books.get(i).getId()==id){
                books.remove(books.get(i));
                return true;
            }
        }
        return false;
    }
    public boolean updateBook(Book book){
        for(int i=0;i < books.size();i++){
            if(books.get(i).getId()==book.getId()){
                books.set(i,book);
                return true;
            }
        }
        return false;
    }
    public Integer count(){
        return books.size();
    }
}
