package com.example.ppmazer.Service;

import com.example.ppmazer.Model.Book;

import java.util.List;
import java.util.Map;

public interface BookService {
    List<Book> getBooks();
    boolean updateBook(Book book);
    boolean deleteBook(int id);
    boolean addBook(Book book);
    Book searchBook(int id);
    Integer count();
}
