package com.example.ppmazer.Service.BookServiceImp;

import com.example.ppmazer.Model.Book;
import com.example.ppmazer.Repository.BookRepository;
import com.example.ppmazer.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BookServiceImpl implements BookService{
    private BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getBooks() {
        return this.bookRepository.getAllBooks();
    }

    @Override
    public boolean updateBook(Book book) {
        return this.bookRepository.updateBook(book);
    }

    @Override
    public boolean deleteBook(int id) {
        return this.bookRepository.removeBook(id);
    }

    @Override
    public boolean addBook(Book book) {
        return this.bookRepository.addBook(book);
    }

    @Override
    public Book searchBook(int id) {
        return this.bookRepository.searchBook(id);
    }

    @Override
    public Integer count() {
        return this.bookRepository.count();
    }

}
