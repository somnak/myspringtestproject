$(document).ready(function () {
    $('body').on('click', '.delete-btn', function () {
        var data_id = $(this).attr("data-id");

        if(confirm("Do you want to delete?")) {
            window.location = "http://localhost:8080/remove/" + data_id;
        }

    });
});